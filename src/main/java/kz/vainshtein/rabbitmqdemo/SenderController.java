package kz.vainshtein.rabbitmqdemo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("send")
@RequiredArgsConstructor
@Slf4j
public class SenderController {

    private final RabbitTemplate rabbitTemplate;

    @GetMapping
    public void send(@RequestParam(name = "text") String text) {
        log.info("Send message: {}", text);
        rabbitTemplate.convertAndSend("testExchange", "testRoute", text);
    }
}
